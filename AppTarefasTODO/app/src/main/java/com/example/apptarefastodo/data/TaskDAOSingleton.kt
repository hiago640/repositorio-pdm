package com.example.apptarefastodo.data

import android.content.Context
import com.example.apptarefastodo.data.database.DBProcessor
import com.example.apptarefastodo.model.Task

object TaskDAOSingleton {

    private lateinit var taskRegistered: ArrayList<Task>
    private lateinit var database: DBProcessor

    private fun startDAO(ctx: Context) {
        if(!::taskRegistered.isInitialized) {
            this.database = DBProcessor(ctx)
            this.taskRegistered = database.read()
        }
    }

    //CRUD METHODS
    //C - CREATE -> Cria uma nova tarefa
    //R - READ -> mostra todas as tarefas
    //U - UPDATE -> atualiza a tarefa
    //D - DELETE -> exclui a tarefa

     fun create(ctx: Context, task: Task): Int {
        this.startDAO(ctx)

         this.taskRegistered.add(0,task)
         task.id = this.database.create(task)
         return 0

         /*
          * task.id = serial++
          * this.taskRegistered.add(0,task)
          * return 0
          */

     }

    fun read(ctx: Context) : ArrayList<Task> {
        startDAO(ctx)

        return taskRegistered

        /*
         * return taskRegistered
         */
    }

     fun update(ctx: Context, task: Task, isCompleted: Boolean): Int {
         startDAO(ctx)

         val position = taskRegistered.indexOf(task)
         this.taskRegistered.get(position).setCompleted(isCompleted)
         return position
    }

    fun delete(ctx: Context, task: Task): Int {
        startDAO(ctx)

        val position = taskRegistered.indexOf(task)
        taskRegistered.removeAt(position)
        database.delete(task)

        return position

        /*
         val position = this.taskRegistered.indexOf(task)
         this.taskRegistered.removeAt(position)
         return position
         */
    }

}