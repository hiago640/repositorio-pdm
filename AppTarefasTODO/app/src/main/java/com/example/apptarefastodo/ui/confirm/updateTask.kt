package com.example.apptarefastodo.ui.confirm

import android.app.AlertDialog
import android.content.Context
import android.widget.Switch


abstract class InputTaskUpdateDialog(private val context: Context) {
    private val switch = Switch(this.context)
    private var isChecked: Boolean? = null

    init {
        this.switch.setOnCheckedChangeListener {
                _, isChecked -> this.isChecked = isChecked
        }

        AlertDialog.Builder(this.context)
            .setTitle("Update the Task")
            .setMessage("Is the task completed?")
            .setView(switch)
            .setPositiveButton("Yes") { _, _ ->
                this.onClickOk(this.isChecked!!)
            }
            .create().show()
    }

    abstract fun onClickOk(isCompleted: Boolean)
}