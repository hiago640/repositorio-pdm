package com.example.apptarefastodo.ui.lists

import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apptarefastodo.R
import com.example.apptarefastodo.model.Task

class TaskListAdapter(
    private var taskList: ArrayList<Task>
) : RecyclerView.Adapter<TaskListHelper>() {

    private var listHelper: ArrayList<Task> = this.taskList
    val tasksSelecteds: SparseArray<Task> = SparseArray()
    var lastTask: Task? = null

    //metodos abstrados que devem ser reescrevidos

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListHelper {
        val inflater = LayoutInflater.from(parent.context)
        //initCardTask(viewType) define qual view deve ser renderizado dependendo se a task é urgente ou não
        val itemView = inflater.inflate(initCardTask(viewType), parent, false)

        return TaskListHelper(itemView, this)
    }

    override fun onBindViewHolder(helper: TaskListHelper, position: Int) {
        helper.bind(this.taskList[position])
    }

    override fun getItemCount(): Int {
        return this.taskList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (this.taskList[position].getIsUrgent()) {/*Urgent*/ 1 } else {/* Not Urgent*/ 0}
    }

    fun filterTasks(filtred: Boolean) {
        if (!filtred) {
            this.taskList = this.listHelper
        } else {
            this.taskList = this.taskList.filter {
                !it.getStatus()
            } as ArrayList<Task>
        }
        this.notifyDataSetChanged()
    }

    private fun initCardTask(viewType: Int): Int {
        return if (viewType == 1) {
            R.layout.task_urgent
        } else {
            R.layout.task_not_urgent
        }
    }


    //LISTENERS - OUVIDORES DE EVENTO


    //Declaração dos listeners
    private var completeListener: CompletedTasksListener? = null
    private var tasksSelectedsListener: ChangeTasksSelectedsListener? = null


    private var listener: ClickTaskListener? = null

    //Construção das interfaces Listeners

    //  ------------------------------------------------------

    fun interface CompletedTasksListener {
        fun whenCompletedTask(task: Task)
    }

    fun setListenerWhenTaskIsComplete(completeListener: CompletedTasksListener): TaskListAdapter {
        this.completeListener = completeListener
        return this
    }

    fun getListenerWhenTaskIsComplete(): CompletedTasksListener? {
        return this.completeListener
    }

    //  ------------------------------------------------------
    fun interface ClickTaskListener {
        fun whenClickTasks(task: Task)
    }

    fun getListenerWhenClickInTask(): ClickTaskListener? {
        return this.listener
    }

    //  ------------------------------------------------------

    fun interface ChangeTasksSelectedsListener {
        fun whenChangeTasksSelectedsList(selectedIds: SparseArray<Task>)
    }

    fun setListenerWhenUpdateSelection(tasksSelectedsListener: ChangeTasksSelectedsListener?): TaskListAdapter {
        this.tasksSelectedsListener = tasksSelectedsListener
        return this
    }

    fun getListenerWhenUpdateSelection(): ChangeTasksSelectedsListener? {
        return this.tasksSelectedsListener
    }


}
