package com.example.apptarefastodo.ui.confirm

import android.app.AlertDialog
import android.content.Context

abstract class DeleteTaskConfirmation(private val context: Context) {
    init {
        AlertDialog.Builder(this.context)
            .setTitle("Delete Task")
            .setMessage("Delete?")
            .setPositiveButton("Yes") { _, _ ->
                this.onConfirm()
            }
            .setNegativeButton("No", null)
            .create().show()

    }

    abstract fun onConfirm()
}