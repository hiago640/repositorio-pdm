package com.example.apptarefastodo.ui.lists

import android.util.SparseArray
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.apptarefastodo.R
import com.example.apptarefastodo.model.Task

class TaskListHelper(
    view: View,
    private val taskListAdapter: TaskListAdapter
) : RecyclerView.ViewHolder(view),
    View.OnLongClickListener,
    View.OnClickListener{

    private lateinit var task: Task
    private val inputNameTask: TextView = view.findViewById(R.id.taskName)
    private val urgencyCheckbox: CheckBox = view.findViewById(R.id.donedTask)

    init {
        this.urgencyCheckbox.setOnCheckedChangeListener { _, isChecked ->this.task.setCompleted(isChecked)
            this.taskListAdapter.getListenerWhenTaskIsComplete()?.whenCompletedTask(this.task)
        }
        view.setOnClickListener(this)
        view.setOnLongClickListener(this)
    }

    override fun onClick(v: View?) {
        this.taskListAdapter.getListenerWhenClickInTask()?.whenClickTasks(this.task)
        this.taskListAdapter.lastTask = this.task
    }

    override fun onLongClick(v: View?): Boolean {
        itemView.isSelected = !itemView.isSelected

        var tasksSelecteds = this.taskListAdapter.tasksSelecteds;
        taskHelper(tasksSelecteds);

        val auxArrayList = this.taskListAdapter.tasksSelecteds.clone()
        this.taskListAdapter.getListenerWhenUpdateSelection()?.whenChangeTasksSelectedsList(auxArrayList)
        return true
    }

    private fun taskHelper(tasksSelecteds: SparseArray<Task>) {
        if (tasksSelecteds.get(this.task.id.toInt()) != null)
            tasksSelecteds.delete(this.task.id.toInt())
        else
            tasksSelecteds.append(this.task.id.toInt(), this.task)
    }


    fun bind(task: Task) {
        this.task = task
        this.inputNameTask.text = this.task.getTaskName()
        this.urgencyCheckbox.isChecked = this.task.getStatus()
        itemView.isSelected = this.taskListAdapter.tasksSelecteds.get(this.task.id.toInt()) != null
    }
}
