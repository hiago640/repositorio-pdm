package com.example.apptarefastodo.data.database

object DBSchema {
    object TableTask {
        const val                   TABLE_NAME = "tasks"
        const val                   ID = "task_id"
        const val                   TASK_NAME = "taskname"
        const val                   IS_URGENT = "isurgent"
        const val                   COMPLETED = "completed"
        const val                   TIMESTAMP = "timestamp"


        fun createTable(): String {
            return """
                CREATE TABLE $TABLE_NAME (
                    $ID          INTEGER     PRIMARY KEY AUTOINCREMENT,
                    $TASK_NAME   TEXT        NOT NULL,
                    $IS_URGENT   INTEGER     NOT NULL,
                    $COMPLETED   INTEGER     DEFAULT 0,
                    $TIMESTAMP   TEXT        DEFAULT CURRENT_TIMESTAMP
                );
            """.trimIndent()
        }
    }
}