package com.example.apptarefastodo.data.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.example.apptarefastodo.model.Task

class DBProcessor (ctx: Context){

    companion object {
        const val WHERE_CLAUSE = "${DBSchema.TableTask.ID} = ?"
        private val table = DBSchema.TableTask;

        val COLUMNS = arrayOf(
            DBSchema.TableTask.ID,
            DBSchema.TableTask.TASK_NAME,
            DBSchema.TableTask.IS_URGENT,
            DBSchema.TableTask.COMPLETED
        )
        val ORDER_BY = "${DBSchema.TableTask.TIMESTAMP} DESC"
    }

    private val dbHelper = DBHelper(ctx)
    private val table = DBSchema.TableTask;

    fun create(task: Task): Long {
        val cv = ContentValues()
            cv.put(DBSchema.TableTask.TASK_NAME, task.getTaskName())
            cv.put(DBSchema.TableTask.IS_URGENT, if(task.getIsUrgent()){1}else{0})

        val db = this.dbHelper.writableDatabase
            val id = db.insert(DBSchema.TableTask.TABLE_NAME, null, cv)
        db.close()
        return id
    }

    fun delete(task: Task) {
        val db = this.dbHelper.writableDatabase

        db.delete(DBSchema.TableTask.TABLE_NAME, WHERE_CLAUSE, arrayOf(task.id.toString()))
        db.close()
    }

    fun update(task: Task) {
        val db = this.dbHelper.writableDatabase

        val cv = ContentValues()
        cv.put(DBSchema.TableTask.COMPLETED, if(task.getStatus()){1}else{0})

        db.update(
            DBSchema.TableTask.TABLE_NAME,
            cv,
            WHERE_CLAUSE,
            arrayOf(task.id.toString())
        )
        db.close()
    }

    fun read(): ArrayList<Task> {
        val taskList: ArrayList<Task> = ArrayList()
        val db = this.dbHelper.writableDatabase

        val cursor = db.query(
            DBSchema.TableTask.TABLE_NAME,
            COLUMNS,
            null,
            null,
            null,
            null, ORDER_BY)

        while(cursor.moveToNext()) {
            readTaskHelper(cursor, taskList)
        }
        db.close()
        return taskList
    }

    private fun readTaskHelper(cursor: Cursor,tasks: ArrayList<Task>) {
        //true = 1 false = 0
        val task = Task(
            cursor.getLong(cursor.getColumnIndexOrThrow(DBSchema.TableTask.ID)),
            cursor.getString(cursor.getColumnIndexOrThrow(DBSchema.TableTask.TASK_NAME)),
            cursor.getString(cursor.getColumnIndexOrThrow(DBSchema.TableTask.IS_URGENT)) == "1",
            cursor.getString(cursor.getColumnIndexOrThrow(DBSchema.TableTask.COMPLETED)) == "1"
        )
        tasks.add(task)
    }


}