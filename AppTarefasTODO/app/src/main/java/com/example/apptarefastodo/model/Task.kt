package com.example.apptarefastodo.model

class Task (
    var isUrgent: Boolean,
    private var taskName: String,
    private var completed: Boolean
)
{
    var id: Long = 0

    constructor(id: Long, taskName: String, isUrgent: Boolean, status: Boolean) : this(isUrgent,taskName, status) {
        this.id = id
        this.taskName = taskName
        this.isUrgent = isUrgent
        this.completed = status
    }

    fun getTaskName(): String {
        return this.taskName
    }

    fun getIsUrgent(): Boolean {
        return this.isUrgent
    }

    fun setCompleted(completed: Boolean) {
        this.completed = completed
    }

    fun getStatus(): Boolean {
        return this.completed
    }


    override fun toString(): String {
        return "Task {isUrgent = $isUrgent, taskName = '$taskName'}"
    }
}