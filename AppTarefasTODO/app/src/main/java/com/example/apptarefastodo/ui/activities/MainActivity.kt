package com.example.apptarefastodo.ui.activities

import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.forEach
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apptarefastodo.R
import com.example.apptarefastodo.data.TaskDAOSingleton
import com.example.apptarefastodo.model.Task
import com.example.apptarefastodo.ui.confirm.DeleteTaskConfirmation
import com.example.apptarefastodo.ui.confirm.InputTaskUpdateDialog
import com.example.apptarefastodo.ui.lists.TaskListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var botaoOK: Button
    private lateinit var switchUrgent: Switch
    private lateinit var completedTasks: CheckBox
    private lateinit var txtInputTask: EditText
    private lateinit var reciclerView: RecyclerView
    private lateinit var taskListAdapter: TaskListAdapter
    private lateinit var fabDeleteTask: FloatingActionButton
    private lateinit var fabEditTask: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.reciclerView = findViewById(R.id.reciclerView)
        this.botaoOK = findViewById(R.id.btnOk)
        this.completedTasks = findViewById(R.id.doneTasks)
        this.switchUrgent = findViewById(R.id.switchUrgent)
        this.txtInputTask = findViewById(R.id.txtInputTas)
        this.fabDeleteTask = findViewById(R.id.fabDeleteTask)
        this.fabEditTask = findViewById(R.id.fabDeleteTask2)


        this.reciclerView.layoutManager = LinearLayoutManager(this)
        this.reciclerView.setHasFixedSize(true)
        this.reciclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        this.taskListAdapter = TaskListAdapter(TaskDAOSingleton.read(baseContext))
            .setListenerWhenUpdateSelection { showOrHideButton(it) }
            /*.setListenerWhenTaskIsComplete {
                val pos = TaskDAOSingleton.update(baseContext, it, this.switchUrgent.isChecked)
            }*/

        this.reciclerView.adapter = this.taskListAdapter

        this.completedTasks?.setOnCheckedChangeListener { _, isChecked ->
            this.taskListAdapter.filterTasks(
                !isChecked
            )
        }

    }

    fun submitButton(v: View) {
        val txtTask: String = this.txtInputTask.text.toString();
        Log.d("txtTask", txtTask.toString());

        if (txtTask.isNotEmpty()) {
            val position = TaskDAOSingleton.create(baseContext,
                Task(this.switchUrgent.isChecked, txtTask, false)
            )
            this.reciclerView.adapter?.notifyItemInserted(position)
            this.reciclerView.scrollToPosition(position)
        }

        reset()
    }

    fun deleteTask(v: View) {
        object : DeleteTaskConfirmation(this) {
            override fun onConfirm() {
                taskListAdapter.tasksSelecteds.forEach { _, value ->
                    val position = TaskDAOSingleton.delete(baseContext,value)
                    taskListAdapter.notifyItemRemoved(position)
                }
                taskListAdapter.tasksSelecteds.clear()
                fabDeleteTask.visibility = View.GONE
            }
        }
    }

    fun updateTask(v: View) {
        object : InputTaskUpdateDialog(this) {
            override fun onClickOk(isCompleted: Boolean) {
                taskListAdapter.tasksSelecteds.forEach { _, value ->

                    val position = TaskDAOSingleton.update(baseContext,value, isCompleted)
                    taskListAdapter.notifyItemRemoved(position)
                }
                taskListAdapter.tasksSelecteds.clear()
                fabDeleteTask.visibility = View.GONE
            }
        }
    }

    private fun reset() {
        this.txtInputTask?.setText("")
        this.switchUrgent?.isChecked = false
    }

    private fun showOrHideButton(array: SparseArray<Task>) {
        if (array.size() > 0) {
            this.fabEditTask.visibility = View.VISIBLE
            this.fabDeleteTask.visibility = View.VISIBLE
        } else {
            this.fabDeleteTask.visibility = View.GONE
            this.fabEditTask.visibility = View.GONE
        }
    }


}
